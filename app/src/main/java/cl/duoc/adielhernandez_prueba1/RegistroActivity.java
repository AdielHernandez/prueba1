package cl.duoc.adielhernandez_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;




public class RegistroActivity extends AppCompatActivity {

    EditText etUsuarioReg, etClaveReg, etRepClaveReg;
    Button btnCrearUsuario, btnVolverLogin;


    BaseDeDatos formularios = new BaseDeDatos();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


        etUsuarioReg = (EditText) findViewById(R.id.etUsuarioReg);
        etClaveReg = (EditText) findViewById((R.id.etClaveReg));
        etRepClaveReg = (EditText) findViewById(R.id.etRepetirClaveReg);
        btnCrearUsuario = (Button) findViewById(R.id.btnCrearUsuario);
        btnVolverLogin = (Button) findViewById(R.id.btnVolverLogin);

        btnCrearUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etClaveReg.toString().equals(etRepClaveReg.toString())) {

                    guardarRegistro();

                    Toast.makeText(RegistroActivity.this, "Usuario agregado con exito", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(RegistroActivity.this, LoginActivity.class);
                    startActivity(i);
                } else {
                    Toast.makeText(RegistroActivity.this, "Las contraseñas ingresadas no coinciden", Toast.LENGTH_LONG).show();
                }

            }
        });

        btnVolverLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegistroActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });


    }

    private void guardarRegistro() {


        if (etUsuarioReg.getText().toString().length() < 5) {
            Toast.makeText(RegistroActivity.this, "El usuario debe tener minimo 6 caracteres", Toast.LENGTH_LONG).show();

        }
        if (etClaveReg.getText().toString().length() < 5) {
            Toast.makeText(RegistroActivity.this, "contraseña debe tener minimo 6 caracteres", Toast.LENGTH_LONG).show();

        }
        if (etClaveReg.toString().equals(etRepClaveReg.toString())) {


        }




    }



}







