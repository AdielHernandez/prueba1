package cl.duoc.adielhernandez_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText etClave, etUsuario;
    Button btnEntrar, btnRegistrarse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnEntrar= (Button)findViewById(R.id.btnEntrar);
        btnRegistrarse=(Button)findViewById(R.id.btnRegistrarse);

       btnEntrar.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               if (v.getId() == R.id.btnEntrar) {

                   if (etUsuario.getText().toString().equals("admin") &&
                           etClave.getText().toString().equals("admin")) {
                       Toast.makeText(LoginActivity.this, "Usuario Valido", Toast.LENGTH_LONG).show();

                   } else {

                       Toast.makeText(LoginActivity.this, "Usuario no valido", Toast.LENGTH_LONG).show();

                   }

               }else if (v.getId() == R.id.btnRegistrarse){
                   Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                   startActivity(i);
               }


           }
       });

        btnRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(i);

            }
        });
    }
}
