package cl.duoc.adielhernandez_prueba1;

/**
 * Created by DUOC on 22-04-2017.
 */

public class ListaUsuarios {

    private String NomUsuario;

    public String getNomUsuario() {
        return NomUsuario;
    }

    public void setNomUsuario(String nomUsuario) {
        NomUsuario = nomUsuario;
    }

    public ListaUsuarios(String nomUsuario) {
        NomUsuario = nomUsuario;
    }
}
