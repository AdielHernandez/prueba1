package cl.duoc.adielhernandez_prueba1;

/**
 * Created by DUOC on 22-04-2017.
 */

public class Usuario {
    private String usuario, Clave, RepClave;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return Clave;
    }

    public void setClave(String clave) {
        Clave = clave;
    }

    public String getRepClave() {
        return RepClave;
    }

    public void setRepClave(String repClave) {
        RepClave = repClave;
    }

    public Usuario(String usuario, String clave, String repClave) {
        this.usuario = usuario;
        Clave = clave;
        RepClave = repClave;
    }
}
